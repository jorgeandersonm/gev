<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>

<html>
<head>
<script>
function dateMask(inputData, e){
	if(document.all) // Internet Explorer
	var tecla = event.keyCode;
	else //Outros Browsers
	var tecla = e.which;

	if(tecla >= 47 && tecla < 58){ // numeros de 0 a 9 e "/"
	var data = inputData.value;
	if (data.length == 2 || data.length == 5){
	data += '/';
	inputData.value = data;
	}
	}else if(tecla == 8 || tecla == 0) // Backspace, Delete e setas direcionais(para mover o cursor, apenas para FF)
	return true;
	else
	return false;
	}
</script>
<title>Adicionar Evento</title>
<jsp:include page="../fragments/htmlHead.jsp" />
</head>
<body>
	<div id="container" style="width: 1000px; margin: 0 auto;">
		<jsp:include page="../fragments/header.jsp" />

		<form:form servletRelativeAction="/evento/adicionar" method="post"
			modelAttribute="evento" role="form" class="form-horizontal">
			<div class="form-group" style="text-align: center;">
				<label class="control-label" style="font-size: 20px;">Adicionar
					Evento</label>
			</div>

			<div class="form-group">
				<label for="nome" class="col-sm-1 control-label">Nome</label>
				<div class="col-sm-10">
					<form:input id="nome" class="form-control" placeholder="Nome"
						path="nome" />
					<form:errors path="nome" cssClass="error" />
				</div>
			</div>

			<div class="form-group">
				<label for="atracoes" class="col-sm-1 control-label">Atrações</label>
				<div class="col-sm-10">
					<form:input id="atracoes" class="form-control"
						placeholder="Atrações" path="atracoes" />
					<form:errors path="atracoes" cssClass="error" />
				</div>
			</div>

			<div class="form-group">
				<label for="data" class="col-sm-1 control-label">Data</label>
				<div class="col-sm-10">
					<form:input id="data" class="form-control" placeholder="Data" type="text" maxlength="10" onkeypress="return dateMask(this, event);"
						path="data" />
					<form:errors path="data" cssClass="error" />
				</div>
			</div>
			
			<div class="form-group">
				<label for="horario" class="col-sm-1 control-label">Horário</label>
				<div class="col-sm-10">
					<form:input id="horario" class="form-control" placeholder="Horário"
						path="horario" />
					<form:errors path="horario" cssClass="error" />
				</div>
			</div>

			<div class="form-group">
				<label for="informacoes" class="col-sm-1 control-label">Informações</label>
				<div class="col-sm-10">
					<form:input id="informacoes" class="form-control"
						placeholder="Informações" path="informacoes" />
					<form:errors path="informacoes" cssClass="error" />
				</div>
			</div>

			<div class="form-group">
				<label for="local" class="col-sm-1 control-label">Local</label>
				<div class="col-sm-10">
					<form:select id="local" class="form-control" path="local"
						items="${locais}" itemLabel="nome" itemValue="id"/>
				</div>
			</div>
			
			<form:input path="usuario" type="hidden" id="usuario" value="${id_usuario}"/>

			<div class="controls">
				<input id="criar" class="btn btn-primary" type="submit"
					value="Adicionar" /> <a
					href="<c:url value="/evento/listar"></c:url>"
					class="btn btn-default">Cancelar</a>
			</div>
		</form:form>
		<jsp:include page="../fragments/footer.jsp" />
	</div>
</body>
</html>