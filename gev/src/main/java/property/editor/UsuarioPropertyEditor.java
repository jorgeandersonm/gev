package property.editor;

import java.beans.PropertyEditorSupport;

import com.jorge.model.Usuario;
import com.jorge.service.UsuarioService;

public class UsuarioPropertyEditor extends PropertyEditorSupport{

	private UsuarioService usuarioService;

	public UsuarioPropertyEditor(UsuarioService usuarioService) {
		super();
		this.usuarioService = usuarioService;
	}
	
	@Override
    public void setAsText(String text) {
        Long id = new Long(text);
        Usuario usuario = usuarioService.getUsuarioById(id);
        super.setValue(usuario);
    }
}
