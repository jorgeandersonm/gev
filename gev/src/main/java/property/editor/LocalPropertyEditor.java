package property.editor;

import java.beans.PropertyEditorSupport;

import com.jorge.model.Local;
import com.jorge.service.LocalService;

public class LocalPropertyEditor extends PropertyEditorSupport {
	
	private LocalService localService;

	public LocalPropertyEditor(LocalService localService) {
		super();
		this.localService = localService;
	}
	
	@Override
    public void setAsText(String text) {
        Long id = new Long(text);
        Local local = localService.getLocalById(id);
        super.setValue(local);
    }

}
