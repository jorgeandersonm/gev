package com.jorge.repository;

import com.jorge.model.Local;

public interface LocalRepository extends GenericRepository<Local> {
	
	public abstract Local getLocalByNome(String nome);
	
	public abstract Local getOutroLocalByNome(Integer id, String nome);

	public abstract Local getLocalById(Long id);

}