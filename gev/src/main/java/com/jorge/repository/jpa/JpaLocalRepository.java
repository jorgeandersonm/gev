package com.jorge.repository.jpa;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Named;

import com.jorge.enumeration.QueryType;
import com.jorge.model.Local;
import com.jorge.repository.LocalRepository;

@Named
public class JpaLocalRepository extends GenericRepositoryImpl<Local> implements
		LocalRepository {

	@Override
	public Local getLocalByNome(String nome) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("nome", nome);
		List<Local> result = find(QueryType.JPQL,
				"from Local where nome = :nome", params);
		if (result != null && !result.isEmpty()) {
			return result.get(0);
		}
		return null;
	}

	@Override
	public Local getOutroLocalByNome(Integer id, String nome) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("nome", nome);
		params.put("id", id);
		List<Local> result = find(QueryType.JPQL,
				"from Local where id != :id and nome = :nome", params);
		if (result != null && !result.isEmpty()) {
			return result.get(0);
		}
		return null;
	}

	@Override
	public Local getLocalById(Long id) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id_local", id);
		List<Local> result = find(QueryType.JPQL,
				"from Local where id_local = :id_local", params);
		if (result != null && !result.isEmpty()) {
			return result.get(0);
		}
		return null;
	}

}
