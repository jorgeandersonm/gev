package com.jorge.repository.jpa;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Named;

import com.jorge.enumeration.QueryType;
import com.jorge.model.Usuario;
import com.jorge.repository.UsuarioRepository;

@Named
public class JpaUsuarioRepository extends GenericRepositoryImpl<Usuario>
		implements UsuarioRepository {

	@Override
	public Usuario getUsuarioByNome(String nome) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("nome", nome);
		List<Usuario> result = find(QueryType.JPQL,
				"from Usuario where nome = :nome", params);
		if (result != null && !result.isEmpty()) {
			return result.get(0);
		}
		return null;
	}

	@Override
	public Usuario getOutroUsuarioByNome(Integer id, String nome) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("nome", nome);
		params.put("id", id);
		List<Usuario> result = find(QueryType.JPQL,
				"from Usuario where id != :id and nome = :nome", params);
		if (result != null && !result.isEmpty()) {
			return result.get(0);
		}
		return null;
	}

	@Override
	public Usuario getUsuarioById(Long id) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", id);
		List<Usuario> result = find(QueryType.JPQL,
				"from Usuario where id = :id", params);
		if (result != null && !result.isEmpty()) {
			return result.get(0);
		}
		return null;
	}
}
