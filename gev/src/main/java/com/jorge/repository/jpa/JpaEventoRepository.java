package com.jorge.repository.jpa;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Named;

import com.jorge.enumeration.QueryType;
import com.jorge.model.Evento;
import com.jorge.repository.EventoRepository;

@Named
public class JpaEventoRepository extends GenericRepositoryImpl<Evento> implements EventoRepository {

	@Override
	public Evento getEventoByNome(String nome) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("nome", nome);
		List<Evento> result = find(QueryType.JPQL, "from Evento where nome = :nome", params);
		if (result != null && !result.isEmpty()) {
			return result.get(0);
		}
		return null;
	}



	@Override
	public Evento getOutroEventoByNome(Integer id, String nome) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("nome", nome);
		params.put("id", id);
		List<Evento> result = find(QueryType.JPQL,
				"from Evento where id != :id and nome = :nome", params);
		if (result != null && !result.isEmpty()) {
			return result.get(0);
		}
		return null;
	}


}
