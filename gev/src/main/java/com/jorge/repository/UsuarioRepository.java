package com.jorge.repository;

import com.jorge.model.Usuario;

public interface UsuarioRepository extends GenericRepository<Usuario> {
	
public abstract Usuario getUsuarioByNome(String nome);
	
	public abstract Usuario getOutroUsuarioByNome(Integer id, String nome);

	public abstract Usuario getUsuarioById(Long id);

}
