package com.jorge.repository;

import com.jorge.model.Evento;

public interface EventoRepository extends GenericRepository<Evento> {
	
	public abstract Evento getEventoByNome(String nome);
	
	
	public abstract Evento getOutroEventoByNome(Integer id, String nome);
	

}