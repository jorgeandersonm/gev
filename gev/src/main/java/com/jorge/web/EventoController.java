package com.jorge.web;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import property.editor.LocalPropertyEditor;
import property.editor.UsuarioPropertyEditor;

import com.jorge.model.Evento;
import com.jorge.model.EventosJson;
import com.jorge.model.Local;
import com.jorge.model.Usuario;
import com.jorge.service.EventoService;
import com.jorge.service.LocalService;
import com.jorge.service.UsuarioService;

@Controller
@RequestMapping("/evento")
public class EventoController {

	@Inject
	private EventoService eventoService;

	@Inject
	@Autowired
	private LocalService localService;
	

	@Autowired
	@Inject
	private UsuarioService usuarioService;	
	
	private DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

	@InitBinder
	protected void initBinder(HttpServletRequest request,ServletRequestDataBinder binder) throws Exception {
		binder.registerCustomEditor(Local.class, new LocalPropertyEditor(localService));
		binder.registerCustomEditor(Usuario.class, new UsuarioPropertyEditor(usuarioService));
	}

	@RequestMapping(value = "/listar")
	public String listar(ModelMap modelMap) {
		modelMap.addAttribute("eventos", this.eventoService.find(Evento.class));
		return "evento/listar";
	}

	@RequestMapping(value = "/{id}/editar", method = RequestMethod.GET)
	public String editar(@PathVariable("id") Integer id, ModelMap modelMap) {

		Evento evento = this.eventoService.find(Evento.class, id);
		modelMap.addAttribute("locais", this.localService.find(Local.class));

		if (evento == null) {
			return "redirect:/evento/listar";

		}
		Long userId = (long) 1;
		
		modelMap.addAttribute("evento", evento);
		modelMap.addAttribute("id_usuario", userId);
		return "evento/editar";
	}

	@RequestMapping(value = "/editar", method = RequestMethod.POST)
	public String atualizar(@Valid Evento evento, BindingResult result,
			RedirectAttributes redirectAttributes) {
		
		if (result.hasErrors()) {
			return "evento/editar";
		}

		eventoService.update(evento);
		redirectAttributes.addFlashAttribute("info",
				"Evento atualizado com sucesso.");
		return "redirect:/evento/listar";

	}

	@RequestMapping(value = "/{id}/excluir", method = RequestMethod.GET)
	public String excluir(@PathVariable("id") Integer id,
			RedirectAttributes redirectAttributes) {
		Evento evento = eventoService.find(Evento.class, id);
		if (evento != null) {
			this.eventoService.delete(evento);
			redirectAttributes.addFlashAttribute("info",
					"Evento removido com sucesso.");
		}
		return "redirect:/evento/listar";
	}

	@RequestMapping(value = "/adicionar")
	public String adicionar(ModelMap modelMap) {
		
		Long userId = (long) 1;
		
		modelMap.addAttribute("id_usuario", userId);
		modelMap.addAttribute("evento", new Evento());
		modelMap.addAttribute("locais", this.localService.find(Local.class));

		return "evento/adicionar";
	}

	@RequestMapping(value = "/adicionar", method = RequestMethod.POST)
	public String adicionar(@Valid Evento evento, BindingResult result,
			RedirectAttributes redirectAttributes, ModelMap modelMap) {

		if (result.hasErrors()) {
			modelMap.addAttribute("locais", this.localService.find(Local.class));
			return "evento/adicionar";
		}

		eventoService.save(evento);
		redirectAttributes.addFlashAttribute("info",
				"Evento adicionado com sucesso.");
		return "redirect:/evento/listar";
	}
	
	@RequestMapping(value = "/json")
	@ResponseBody
	public EventosJson eventos() throws ParseException{
		Usuario usuario = new Usuario();
		List<Evento> eventos = eventoService.find(Evento.class);
		List<Evento> eventosJson = new ArrayList<Evento>();
		usuario.setLogin("jorgeandersonm");

		
		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		Date data = new Date();
		
		Calendar cal = Calendar.getInstance();
		
		cal.getTime();
		cal.add(Calendar.DAY_OF_MONTH, -2);
		
		data = cal.getTime();

		 
		for(int i =0; i < eventos.size(); i++){
			
			Date data2 = (Date)formatter.parse(eventos.get(i).getData()); 
			
			if(data.before(data2)){
				eventos.get(i).setUsuario(usuario);
				eventosJson.add(eventos.get(i));
				
				
			}
		}

		EventosJson ejson = new EventosJson();
		ejson.setEventos(ordenarPorData(eventosJson));
		return ejson;
	}
	
	public List<Evento> ordenarPorData(List<Evento> eventos) throws ParseException{
		
		for(int i = eventos.size()-1; i >= 0; i--) {
			
	        for(int j = 0; j < i; j++) {
	        	Date data2 = (Date)formatter.parse(eventos.get(j).getData()); 
            	Date data1 = (Date)formatter.parse(eventos.get(j+1).getData()); 
	        	
	            if(data2.after(data1)) {
	                Evento temp = eventos.get(j);
	                eventos.set(j, eventos.get(j + 1));
	                eventos.set(j + 1, temp);
	            }
	        }
	    }
		
		
		return eventos;
	}
}
