package com.jorge.web;

import javax.inject.Inject;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.jorge.model.Local;
import com.jorge.service.LocalService;

@Controller
@RequestMapping("/local")
public class LocalController {

	@Inject
	private LocalService localService;

	@RequestMapping(value = "/listar")
	public String listar(ModelMap modelMap) {
		modelMap.addAttribute("locais", this.localService.find(Local.class));
		return "local/listar";
	}

	@RequestMapping(value = "/{id}/editar", method = RequestMethod.GET)
	public String editar(@PathVariable("id") Integer id, ModelMap modelMap) {

		Local local = this.localService.find(Local.class, id);

		if (local == null) {
			return "redirect:/local/listar";

		}
		modelMap.addAttribute("local", local);
		return "local/editar";
	}

	@RequestMapping(value = "/editar", method = RequestMethod.POST)
	public String atualizar(@Valid Local local, BindingResult result, RedirectAttributes redirectAttributes) {
		if (result.hasErrors()) {
			return "local/editar";
		}
		localService.update(local);
		redirectAttributes.addFlashAttribute("info", "Local atualizado com sucesso.");
		return "redirect:/local/listar";

	}
	
	@RequestMapping(value = "/{id}/excluir", method = RequestMethod.GET)
	public String excluir(@PathVariable("id") Integer id, RedirectAttributes redirectAttributes) {
		Local local = localService.find(Local.class, id);
		if (local != null) {
			this.localService.delete(local);
			redirectAttributes.addFlashAttribute("info", "Local removido com sucesso.");
		}
		return "redirect:/local/listar";
	}

	@RequestMapping(value = "/adicionar")
	public String adicionar(ModelMap modelMap) {
		modelMap.addAttribute("local", new Local());
		return "local/adicionar";
	}

	@RequestMapping(value = "/adicionar", method = RequestMethod.POST)
	public String adicionar(@Valid Local local, BindingResult result, RedirectAttributes redirectAttributes) {

		if (result.hasErrors()) {
			return "local/adicionar";
		}
		localService.save(local);
		redirectAttributes.addFlashAttribute("info", "Local adicionado com sucesso.");
		return "redirect:/local/listar";
	}
	
}

