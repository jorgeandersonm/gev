package com.jorge.enumeration;

public enum QueryType {
	
	JPQL, NATIVE, NAMED

}
