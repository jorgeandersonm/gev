package com.jorge.service;

import com.jorge.model.Local;

public interface LocalService extends GenericService<Local> {

	public abstract Local getLocalByNome(String nome);

	public abstract Local getOutroLocalByNome(Integer id, String nome);
	
	public abstract Local getLocalById(Long id);

}