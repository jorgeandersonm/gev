package com.jorge.service;

import com.jorge.model.Evento;

public interface EventoService extends GenericService<Evento> {

	public abstract Evento getEventoByNome(String nome);

	public abstract Evento getOutroEventoByNome(Integer id, String nome);

}