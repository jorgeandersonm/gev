package com.jorge.service;

import com.jorge.model.Usuario;

public interface UsuarioService extends GenericService<Usuario> {

	public abstract Usuario getUsuarioByNome(String nome);

	public abstract Usuario getOutroUsuarioByNome(Integer id, String nome);
	
	public abstract Usuario getUsuarioById(Long id);

}
