package com.jorge.service.impl;

import javax.inject.Inject;
import javax.inject.Named;

import com.jorge.model.Usuario;
import com.jorge.repository.UsuarioRepository;
import com.jorge.service.UsuarioService;

@Named
public class UsuarioServiceImpl  extends GenericServiceImpl<Usuario> implements UsuarioService {

	@Inject
	private UsuarioRepository usuarioRepository;
	
	@Override
	public Usuario getUsuarioByNome(String nome) {
		return usuarioRepository.getUsuarioByNome(nome);
	}

	@Override
	public Usuario getOutroUsuarioByNome(Integer id, String nome) {
		return usuarioRepository.getOutroUsuarioByNome(id, nome);
	}

	@Override
	public Usuario getUsuarioById(Long id) {
		return usuarioRepository.getUsuarioById(id);
	}

}
