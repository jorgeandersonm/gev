package com.jorge.service.impl;

import javax.inject.Inject;
import javax.inject.Named;

import com.jorge.model.Evento;
import com.jorge.repository.EventoRepository;
import com.jorge.service.EventoService;

@Named
public class EventoServiceImpl extends GenericServiceImpl<Evento> implements EventoService {
	
	@Inject
	private EventoRepository eventoRepository;

	@Override
	public Evento getEventoByNome(String nome) {
		return eventoRepository.getEventoByNome(nome);
	}


	@Override
	public Evento getOutroEventoByNome(Integer id, String nome) {
		return eventoRepository.getOutroEventoByNome(id, nome);
	}



}