package com.jorge.service.impl;

import javax.inject.Inject;
import javax.inject.Named;

import com.jorge.model.Local;
import com.jorge.repository.LocalRepository;
import com.jorge.service.LocalService;

@Named
public class LocalServiceImpl extends GenericServiceImpl<Local> implements
		LocalService {

	@Inject
	private LocalRepository localRepository;

	@Override
	public Local getLocalByNome(String nome) {
		return localRepository.getLocalByNome(nome);
	}

	@Override
	public Local getOutroLocalByNome(Integer id, String nome) {
		return localRepository.getOutroLocalByNome(id, nome);
	}

	@Override
	public Local getLocalById(Long id) {
		return localRepository.getLocalById(id);
	}

}