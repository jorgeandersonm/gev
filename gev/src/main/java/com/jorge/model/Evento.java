package com.jorge.model;

import java.util.Comparator;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "eventos")
public class Evento implements Comparator<Date>{

	public Evento() {

	}

	public Evento(Integer id, String nome, String atracoes, String horario,
			String informacoes, String data) {
		super();
		this.id = id;
		this.nome = nome;
		this.atracoes = atracoes;
		this.horario = horario;
		this.informacoes = informacoes;
		this.data = data;
	}

	@Id
	@Column(name = "id_nome")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "nome_evento")
	@NotEmpty(message = "Campo obrigatório")
	private String nome;

	@Column(name = "atracoes")
	@NotEmpty(message = "Campo obrigatório")
	private String atracoes;

	@Column(name = "horario")
	@NotEmpty(message = "Campo obrigatório")
	private String horario;

	@Column(name = "informacoes")
	@NotEmpty(message = "Campo obrigatório")
	private String informacoes;

	@Column(name = "data")
	@NotEmpty(message = "Campo obrigatório")
	private String data;
	

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_usuario")
	private Usuario usuario;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_local")
	private Local local;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getAtracoes() {
		return atracoes;
	}

	public void setAtracoes(String atracoes) {
		this.atracoes = atracoes;
	}

	public String getHorario() {
		return horario;
	}

	public void setHorario(String horario) {
		this.horario = horario;
	}

	public String getInformacoes() {
		return informacoes;
	}

	public void setInformacoes(String informacoes) {
		this.informacoes = informacoes;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Local getLocal() {
		return local;
	}

	public void setLocal(Local local) {
		this.local = local;
	}

	@Override
	public String toString() {
		return "Evento [id=" + id + ", nome=" + nome + ", atracoes=" + atracoes
				+ ", horario=" + horario + ", informacoes=" + informacoes
				+ ", data=" + data + ", local="
				+ local + "]";
	}

	@Override
	public int compare(Date one, Date two) {
		// TODO Auto-generated method stub
		return one.compareTo(two);
	}

}
