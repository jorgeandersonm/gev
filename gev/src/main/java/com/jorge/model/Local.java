package com.jorge.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

@Entity
@Table(name = "local")
public class Local implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id_local")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@NotNull(message = "Campo obrigatório")
	@Column(name = "lat")
	@NumberFormat(style = Style.NUMBER)
	private String lat;

	@NotNull(message = "Campo obrigatório")
	@Column(name = "lon")
	@NumberFormat(style = Style.NUMBER)
	private String lon;


	@NotEmpty(message = "Campo obrigatório")
	@Column(name = "nome_l")
	private String nome;
	
	@NotEmpty(message = "Campo obrigatório")
	@Column(name = "cidade_l")
	private String cidade;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getLon() {
		return lon;
	}

	public void setLon(String lon) {
		this.lon = lon;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	@Override
	public String toString() {
		return "Local [id=" + id + ", lat=" + lat + ", lon=" + lon + ", nome="
				+ nome + ", cidade=" + cidade + "]";
	}
}
